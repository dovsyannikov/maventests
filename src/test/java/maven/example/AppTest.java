package maven.example;


import org.testng.Assert;
import org.testng.annotations.Test;


public class AppTest {
    @Test
    public void testHello(){
        App.main(new String[]{});
        Assert.assertEquals("Hello world!", "Hello world!");
    }
    @Test
    public void testHelloSecond(){
        App.main(new String[]{});
        Assert.assertEquals("Hello!", "Hello world!");
    }
}
