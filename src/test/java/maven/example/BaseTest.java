package maven.example;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class BaseTest {
    private WebDriver driver;

    @Test
    public void myFirstTest(){
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("https://the-internet.herokuapp.com");
        driver.manage().window().maximize();

        driver.findElement(By.linkText("Shifting Content")).click();
        driver.findElement(By.partialLinkText("Example 1")).click();
        List<WebElement> menuItems = driver.findElements(By.tagName("li"));
        for (WebElement menu :
                menuItems) {
            System.out.println(menu.getText());
        }
        System.out.println("Number of menu items: " + menuItems.size());
        driver.quit();
    }
    @Test(enabled = true)
    public void test2(){
        driver.get("www.google.com");
    }
}
